"""Limited List
For this exercise, do not use inheritance!
Create a class called LimitedList, that will be initialized with a maximum number of elements it can hold.
After your object has reached its limit, whenever someone tries to append an additional value to the list – the new
member will be appended, and the first member in the list will be erased.
Your object should support append, as well as assignment and receiving items using []. In addition, it should implement
the __repr__ slot.
For example:
>>> my_list = LimitedList(3)
>>> my_list
[]
>>> my_list.append(5)
>>> my_list.append(2)
>>> my_list.append(10)
>>> my_list
[5, 2, 10]
>>> my_list.append("hello")
>>> my_list
[2, 10, 'hello']
>>> my_list[1] = "changed"
>>> my_list
[2, 'changed', 'hello']
Hint: Google __getitem__ and __setitem__."""

class LimitedList():
    def __init__(self, max_length):
        self.max_length = max_length
        self.ls = []

    def __getitem__(self, item):
        if len(self.ls) == self.max_length:
            self.ls.pop(0)
        self.ls.append(item)

    def __setitem__(self):
        return self.ls

    #def __resr__(self):


my_list = LimitedList(3)
print(my_list)